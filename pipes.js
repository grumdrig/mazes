#!/usr/bin/env -S loopy -x -O maze.scad node

function go() {
	let d = 4;
	let inlet =   {x: 1,   y: d-2, z: d - 1};
	let outflow = {x: d-2, y: 0, z: 1    };
	// let m = new GrowingTreeMaze(d, inlet, outflow);
	// m.grid[inlet.z][inlet.y][inlet.x] |= U;
	// m.grid[outflow.z][outflow.y][outflow.x] |= S;
	let m = new BoobleBib(d, inlet, outflow);

	console.log(`
N = 1; S = 2; E = 4; W = 8; U = 16; D = 32;

L = 10;

module rot(d) {
	if (d == U) {
		children();
	} else if (d == D) {
		rotate([180, 0, 0])
		children();
	} else if (d == N) {
		rotate([-90, 0, 0])
		children();
	} else if (d == S) {
		rotate([90, 0, 0])
		children();
	} else if (d == E) {
		rotate([0, 90, 0])
		children();
	} else if (d == W) {
		rotate([0, -90, 0])
		children();
	}
}

module tube(x0, y0, z0, d) {
	translate([x0, y0, z0])
	rot(d)
	cylinder(h = 0.48, d = 1/10);
}

module tube2(x0, y0, z0, d) {
	translate([x0, y0, z0])
	rot(d)
	cylinder(h = 0.96, d = 1/10);
}

module ball(x, y, z) {
	color("red")
	translate([x, y, z])
	sphere(2/10);
}
`);

	m.draw();
}


function rand(n) {
	return Math.floor(Math.random() * n);
}


function shuffle(array) {
	let tmp, current, top = array.length;

	if(top) while(--top) {
		current = Math.floor(Math.random() * (top + 1));
		tmp = array[current];
		array[current] = array[top];
		array[top] = tmp;
	}

	return array;
}



// Growing Tree Algorithm
// http://weblog.jamisbuck.org/2011/1/27/maze-generation-growing-tree-algorithm
//
// 1. Let C be a list of cells, initially empty. Add one cell to C, at
// random.
//
// 2. Choose a cell from C, and carve a passage to any unvisited
// neighbor of that cell, adding that neighbor to C as well. If there
// are no unvisited neighbors, remove the cell from C.
//
// 3. Repeat #2 until C is empty.

const N = 1, S = 2, E = 4, W = 8, U = 16, D = 32;
const DX = {}, DY = {}, DZ = {}, OPPOSITE = {};
DX[E] = 1; DX[W] = -1; DX[N] = DX[S] = DX[U] = DX[D] = 0;
DY[N] = 1; DY[S] = -1; DY[E] = DY[W] = DY[U] = DY[D] = 0;
DZ[U] = 1; DZ[D] = -1; DZ[S] = DZ[N] = DZ[E] = DZ[W] = 0;
OPPOSITE[E] = W;
OPPOSITE[W] = E;
OPPOSITE[N] = S;
OPPOSITE[S] = N;
OPPOSITE[D] = U;
OPPOSITE[U] = D;
const DIRECTIONS = [N, S, E, W, U, D];
const DNAMES = [];
DNAMES[N] = 'N';
DNAMES[S] = 'S';
DNAMES[E] = 'E';
DNAMES[W] = 'W';
DNAMES[U] = 'U';
DNAMES[D] = 'D';


class GrowingTreeMaze {
	constructor(d, inlet, outflow) {
		this.d = d;
		let m = this;
		let grid = this.grid = Array(d).fill().map(_ =>
			Array(d).fill().map(a => Array(d).fill(0)));
		let stack = [];
		let _visited = {};
		function id(c) { return c.x + d * (c.y + d * c.z); }
		function visited(c) { return _visited[id(c)]; }
		function visit(c) {
			_visited[id(c)] = true;
			stack.push(c);
		}
		visit(inlet);
		// visit(outflow);
		while (stack.length > 0) {
			// Choosing a mix of recursive backtracking, vs. randomization. A
			// value of 1 would be Prim's algorithm, 0 would be recursive
			// backtracking.
			let RANDOMIZATION = 1/8;
			let ci = (Math.random() < RANDOMIZATION) ? rand(stack.length) :
				stack.length - 1;
			let c = stack[ci];
			let DS = DIRECTIONS.slice(0);
			shuffle(DS);
			let di, dd, n;
			for (di = 0; di < DIRECTIONS.length; ++di) {
				dd = DS[di];
				n = {	x: c.x + DX[dd],
						y: c.y + DY[dd],
						z: c.z + DZ[dd]};
				if (0 <= n.z && n.z < d &&
					0 <= n.y && n.y < d &&
					0 <= n.x && n.x < d &&
					!visited(n))
					break;
			}
			if (di < DIRECTIONS.length) {
				grid[c.z][c.y][c.x] |= dd;
				grid[n.z][n.y][n.x] |= OPPOSITE[dd];
				visit(n);
				if (id(n) === id(outflow)) break;
			} else {
				stack.splice(ci, 1);
			}
		}
	}

	draw() {
		let m = this;
		for (let z = 0; z < m.d; z += 1)
		for (let y = 0; y < m.d; y += 1)
		for (let x = 0; x < m.d; x += 1)
		for (let d of DIRECTIONS) {
			if (m.grid[z][y][x] & d) {
				console.log(`tube(${x}, ${y}, ${z}, ${DNAMES[d]});`);
			}
		}
	}
}


class BoobleBib {

	constructor(d, start, end) {
		this.d = d;
		this.start = start;
		this.end = end;
		function id(c) { return c.x + d * (c.y + d * c.z); }
		let routes = [];
		for (let z = 0; z < d; z += 1)
		for (let y = 0; y < d; y += 1)
		for (let x = 0; x < d; x += 1)
		for (let dir of [E, N, U]) {
			let n = {	x: x + DX[dir],
						y: y + DY[dir],
						z: z + DZ[dir]  };
			if (0 <= n.z && n.z < d &&
				0 <= n.y && n.y < d &&
				0 <= n.x && n.x < d)
			{
				routes.push([id({x,y,z}), id(n), dir]);
			}
		}

		shuffle(routes);
		this.routes = routes;

		start = id(start);
		end = id(end);

		let changed = 1;
		while (changed) {
			changed = 0;
			for (let i = 0; i < this.routes.length; i += 1) {
				let routes = this.routes.slice(0);
				routes.splice(i, 1);
				if (this.connected(routes, start, end)) {
					this.routes = routes;
					changed += 1
				}
			}
		}
	}

	connected(routes, start, end) {
		let reachable = new Set([start]);
		routes = new Set(routes);
		while (routes.size) {
			let gotsome = false;
			for (let node of routes) {
				let [from, to, dir] = node;
				let gotone = false;
				if (reachable.has(from)) {
					if (to === end) return true;
					reachable.add(to);
					gotone = true;
				}
				if (reachable.has(to)) {
					if (from === end) return true;
					reachable.add(from);
					gotone = true;
				}
				if (gotone) {
					routes.delete(node);
					gotsome = true;
				}
			}
			if (!gotsome) break;
		}
		return false;
	}

	draw() {
		let d = this.d;
		function decode(id) {
			let x = id % d;
			id = (id - x) / d;
			let y = id % d;
			let z = (id - y) / d;
			return {x,y,z};
		}
		for (let [f, t, d] of this.routes) {
			f = decode(f);
			console.log(`tube2(${f.x}, ${f.y}, ${f.z}, ${d});`);
		}
		console.log(`ball(${this.start.x}, ${this.start.y}, ${this.start.z});`);
		console.log(`ball(${this.end.x}, ${this.end.y}, ${this.end.z});`);
	}
}


go();
